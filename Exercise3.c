#include <stdio.h>
#define rows 3
#define cols 3

int main(){

    int matrix1[rows][cols];
    int matrix2[rows][cols];
    int matrixadd[rows][cols];
    int matrixmul[rows][cols];
    printf("Input first %d * %d matrix\n",rows,cols);
    read(matrix1);
    print(matrix1);
    printf("Input Second %d * %d matrix\n",rows,cols);
    read(matrix2);
    print(matrix2);
    add(matrixadd, matrix1,matrix2);
    printf("\nAddition of two matrix \n");
    print(matrixadd);
    mul(matrixmul, matrix1,matrix2);
    printf("\nMultiplication of two matrix\n");
    print(matrixmul);
}

void read(int matrix[rows][cols]){
    for(int i = 0; i < rows; i++){
        printf("input %d row of the matrix : ",i+1);
        for(int j = 0; j <cols; j++ ){
            scanf("%d",&matrix[i][j]);
        }

    }
}

void print(int matrix[rows][cols]){
    for(int i = 0; i < rows; i++){
        for(int j = 0; j <cols; j++ ){
            printf("%d ",matrix[i][j]);
        }
        printf("\n\n");
    }
}

void add(int matrixadd[rows][cols], int matrix1[rows][cols], int matrix2[rows][cols]){
    for(int i = 0; i < rows; i++){
        for(int j = 0; j <cols; j++ ){
            matrixadd[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }
}

void mul(int matrixmul[rows][cols], int a[rows][cols], int b[rows][cols]){
    for(int j = 0; j < cols ; j++){
        for(int i = 0; i < rows; i++){
            matrixmul[i][j] = a[i][0]*b[0][j] + a[i][1]*b[1][j] + a[i][2]*b[2][j] ;
        }
    }
}


